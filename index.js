// Setup the dependencies 
const express = require("express");
const mongoose = require("mongoose");

const taskRoute = require("./routes/taskRoute");

const app = express();
const port = 3000;

app.use(express.json());

// Database Connection
mongoose.connect("mongodb+srv://admin:admin123@batch204-likristoffer.rqyug4v.mongodb.net/B204-to-dos?retryWrites=true&w=majority", 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

let db = mongoose.connection;
//If a connectin error occured, output in the console
db.on("error", console.error.bind(console, "Connection Error"));

//If the connection is successful, output in the console
db.once("open", () => console.log("We're connected to the cloud database."));

// Add the task route
app.use("/tasks", taskRoute);
// localhost:3000/tasks


// Server Listening
app.listen(port, () => console.log(`Now listening to port ${port}`));